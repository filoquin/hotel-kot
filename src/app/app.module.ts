import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { environment } from './../environments/environment';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {HttpClient, HttpClientModule, HttpHeaders} from '@angular/common/http';
import { OdooRPCService } from './services/odoorcp.service';
import { KotComponent } from './kot/kot.component';


@NgModule({
  declarations: [
    AppComponent,
    KotComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule 


  ],
  providers: [OdooRPCService],
  bootstrap: [AppComponent]
})
export class AppModule {
    constructor(public odooRPC:OdooRPCService) { 
        this.odooRPC.init({
            odoo_server: environment.odoo_server,
            http_auth: "username:password" // optional
        });
     }
}
