import { Component, OnInit } from '@angular/core';
import { OdooRPCService } from "../services/odoorcp.service";
import { environment } from './../../environments/environment';
import { timer } from 'rxjs';


@Component({
  selector: 'app-kot',
  templateUrl: './kot.component.html',
  styleUrls: ['./kot.component.css']
})
export class KotComponent implements OnInit {

  public orderList: any = [];
  public total: number = 0;

	
	oberserableTimer() {
	    const source = timer(1000, 2000);
	    const abc = source.subscribe(val => {
	      this.getKotOrders();
	    });
	  }

  constructor(public odooRPC: OdooRPCService) {}


  ngOnInit() {
  		if (environment.production==false){

  			this.odooRPC
			.login(environment.db, environment.user, environment.password)
			.then(res => {
				this.getKotOrders();
				this.oberserableTimer();
			})
			.catch(error => {
				console.log(error);
			});
  		} else {
			this.getKotOrders();
			this.oberserableTimer();

  		}


  }

  getKotOrders(){
	this.odooRPC
			.searchRead(
				"hotel.restaurant.order.list",
				[['state','!=','done'],['kot_order_list','!=',false]],[]
			)
			.then(res => {
				console.log(res);
				this.total = res['length']
				this.orderList = res["records"];
			})
			.catch(error => {
				console.log(error);
			});
  }
  public do_action(pos,action) {
		this.odooRPC
			.call(
				"hotel.restaurant.order.list",
				action,
				[this.orderList[pos]["id"]],
				{}
			)
			.then(res => {
				this.getKotOrders();
			});
	}
}
